Reviving an HP PA-RISC server
2019-04-13

A while ago, I got my hands on a beast of a machine, a 7U HP L3000
(rp5470) PA-RISC server. These were released in the year 2000 and came
with up to 16GB (whoa) of RAM and up to 4 CPUs.

The best site for information on PA-RISC machines is, no doubt,
[OpenPA.net](https://www.openpa.net/systems/hp_l1500_l3000-rp5430_rp5470.html),
and they have a fantastic page on my machine.

This is the story of how I managed to install Gentoo GNU/Linux on this
classic UNIX server.
<!--more-->

OK, maybe classic is too strong a word, but it is a fairly unique
machine. I've written a few posts about SPARC machines and PA-RISC is
in the same vein - a RISC CPU architecture with machines and OS both
sold by a single vendor. In this case, the vendor is HP, the CPU is
has the PA-RISC architecture, and the OS is (or was) HP-UX.

I don't have any disks of HP-UX around and HP doesn't provide them on
their website. Oracle (!!) provides Solaris freely, but I had no such
luck with HP. Using OpenPA.net to work out which OSs were compatible
with my L3000, I eventually settled on Gentoo GNU/Linux.

##The Guardian Service Processor

Similarly to most enterprise servers, there is a kind of service
processor that you can connect to the network and use to access the
console, administrate the server, etc, without powering it on.

HP calls it the Guardian Service Processor or GSP for short.

The first task for me was to reset the password. I had to open up the
server and press the GSP reset button. I then connected the GSP to the
network, determined its IP address, and was able to telnet in. This is
what it looks like:

```
$ telnet gsp.gondolin
Trying 192.168.1.18...
Connected to gsp.gondolin.int.kaashif.co.uk.
Escape character is '^]'.

Service Processor login:
Service Processor password:




             Hewlett-Packard Guardian Service Processor

  (c) Copyright Hewlett-Packard Company 1999-2001.  All Rights Reserved.

                      System Name: gsp



*************************************************************************
                        GSP ACCESS IS NOT SECURE
   No GSP users are currently configured and remote access is enabled.
             Set up a user with a password (see SO command)
                                   OR
       Disable all types of remote access (see EL and ER commands)
*************************************************************************
```

You can just hit enter twice to login without a user or password,
since those were reset.

##Booting from a CD

Luckily, my server has a working CD drive, so I could download the
Gentoo minimal installation CD
(<https://wiki.gentoo.org/wiki/Handbook:HPPA/Installation/Media>), pop
it in, and get started.

The first step is to power on the machine. To do this, I connected to
the console through telnet, CTRL-E then CF to activate console write
access, and CTRL-B to access the GSP prompt:

```
[Read only - use ^Ecf for console write access.]

[bumped user -  ]


Leaving Console Mode - you may lose write access.
When Console Mode returns, type ^Ecf to get console write access.

GSP Host Name:  gsp
GSP>
```

Now you can type `he` for a help menu. The list of commands is:

```
==== GSP Help ============================================(Administrator)===
AC  : Alert display Configuration       MS  : Modem Status
AR  : Automatic System Restart config.  PC  : Remote Power Control
CA  : Configure asynch/serial ports     PG  : PaGing parameter setup
CL  : Console Log- view console history PS  : Power management module Status
CO  : COnsole- return to console mode   RS  : Reset System through RST signal
CSP : Connect to remote Service Proc.   SDM : Set Display Mode (hex or text)
DC  : Default Configuration             SE  : SEssion- log into the system
DI  : DIsconnect remote or LAN console  SL  : Show Logs (chassis code buffer)
EL  : Enable/disable LAN/WEB access     SO  : Security options & access control
ER  : Enable/disable Remote/modem       SS  : System Status of proc. modules
EX  : Exit GSP and disconnect           TC  : Reset via Transfer of Control
HE  : Display HElp for menu or command  TE  : TEll- send a msg. to other users
IT  : Inactivity Timeout settings       VFP : Virtual Front Panel display
LC  : LAN configuration                 WHO : Display connected GSP users
LS  : LAN Status                        XD  : Diagnostics and/or Reset of GSP
MR  : Modem Reset                       XU  : Upgrade the GSP Firmware

====
(HE for main help, enter command name, or Q to quit)
```

The important command is PC, for remote power control. Turn the power
switch on, then execute the PC command to turn it on.

If you're doing this for the first time, you'll need to follow the
instructions in the Gentoo page to install Gentoo. That is out of the
scope of this post.

The interesting parts of the boot process are the hardware detection:

```
Firmware Version  42.06

Duplex Console IO Dependent Code (IODC) revision 1

------------------------------------------------------------------------------
   (c) Copyright 1995-2000, Hewlett-Packard Company, All rights reserved
------------------------------------------------------------------------------

  Processor   Speed            State           CoProcessor State  Cache Size
  Number                                       State              Inst    Data
  ---------  --------   ---------------------  -----------------  ------------
      0      550  MHz   Active                 Functional         512 KB   1 MB

  Central Bus Speed (in MHz)  :        133
  Available Memory            :    2097152  KB
  Good Memory Required        :      25000  KB

   Primary boot path:    0/0/1/1.2
   Alternate boot path:  0/0/2/0.2
   Console path:         0/0/4/1.0
   Keyboard path:        0/0/4/0.0


Processor is booting from first available device.

To discontinue, press any key within 10 seconds.

10 seconds expired.
Proceeding...
```

Then Linux starts booting. There are a ton of errors, but somehow it
boots up fine and gives me a login prompt.

##Can you actually use it for anything?

It would be a really bad idea to use this server for anything
real. It's huge, it's heavy, it has a slow CPU. There's not really
anything special or revolutionary about the CPU architecture, as far
as I can tell. I haven't measured it, but it probably uses a few
thousand watts.

There aren't really any binary packages available for Gentoo, so you
have to compile everything, which is a huge time sink. Debian might be
a better choice in this regard.

Lets get onto the really important question:

##Why not OpenBSD?

There's no support for hppa64! You may wonder, then, how did Linux get
support? HP helped out. They supplied documentation and code,
eventually leading to Debian and Gentoo being ported (and they both
still work on hppa, to this day!).

OpenBSD has support for most workstations, 32 bit and 64 bit running
in 32 bit mode. Server support is a bit lacking, but this is
understandable given the lack of hardware and interest.

##What does the machine code look like?

I'm glad you asked. After snooping around the binaries, I objdumped
some interesting-looking ones. Here's `/bin/sh`:

```

/bin/sh:     file format elf32-hppa-linux


Disassembly of section .init:

000112e4 <.init>:
   112e4:       6b c2 3f d9     stw rp,-14(sp)
   112e8:       6f c4 00 80     stw,ma r4,40(sp)
   112ec:       6b d3 3f c1     stw r19,-20(sp)
   112f0:       e8 40 14 78     b,l 11d34 <_GLOBAL_OFFSET_TABLE_@@Base-0x19fa0>,rp
   112f4:       08 00 02 40     nop
   112f8:       e8 4b 0b a0     b,l 278d0 <_GLOBAL_OFFSET_TABLE_@@Base-0x4404>,rp
   112fc:       08 00 02 40     nop
   11300:       4b c2 3f 59     ldw -54(sp),rp
   11304:       08 04 02 53     copy r4,r19
   11308:       e8 40 c0 00     bv r0(rp)
   1130c:       4f c4 3f 81     ldw,mb -40(sp),r4

Disassembly of section .text:

00011310 <.text>:
   11310:       2b 60 00 00     addil L%0,dp,r1
   11314:       48 35 06 50     ldw 328(r1),r21
   11318:       ea a0 c0 00     bv r0(r21)
   1131c:       48 33 06 58     ldw 32c(r1),r19
   11320:       2b 60 00 00     addil L%0,dp,r1
```

Wow, I don't recognise any of those instructions!

Expect a blog post in the near future (less than a year) explaining
some quirks of PA-RISC. I'm sure there's no shortage of weirdness and
oddities... Maybe there'll even be a cool feature or two not found in
modern processors.

If I ever get my hands on a working copy of HP-UX, expect a post about
that, too.
