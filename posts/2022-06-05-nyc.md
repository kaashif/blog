I'm moving to New York, here's why
2022-06-05

No-one reads this blog, so this is a safe place to make a public
announcement I don't really want anyone to see.

I'll be moving to New York in a few months when my visa goes through.

There are many reasons I'm moving to New York, the main ones are:

* More money

* Looking to run away from my startup failure

* And the main one, I'm looking for something cool to do because
  I'm bored.

Read on for some elaboration.

<!-- more -->

Here's the elaboration: \$\$\$\$\$\$\$\$\$\$\$\$. I think that about sums it up.

But seriously, here are some reasons I want to get out of the UK and
into the US.

## More money

Software engineers are paid significantly more in the US than in the
UK, especially in hubs like San Francisco and New York. It's not even
close. The best career move any software engineer can make is moving
to the US.

Salaries in Europe are generally disgraceful. My salary isn't bad, but
there is no reason I should be leaving hundreds of thousands of
dollars on the table to stay in the UK. The percentage raise from
switching jobs would be significantly less than the raise from just
moving to the US within the same company.

Maybe if I had a family I'd stay here, but I don't. I think that's the
only case where millions or billions of dollars in lifetime earnings
is a price people would actually pay.

## Running away from my problems

I tried to be a solo startup founder for a bit, all my ideas were
ultimately stupid and I went about evaluating them wrong way (see
earlier blog posts on this).

I then tried to co-found a payments startup with someone I
communicated with mainly online, but that didn't work out since we
just weren't close enough to really trust each other or be able to
support each other. That meant as things got worse, neither of us
really had anyone to really open up to. Or, at least, I didn't.

After that didn't work out, I went back to my old job with the express
purpose of moving to the US.

The problem I'm trying to fix is that it feels like I took a huge leap
into the unknown (founding a startup), cracked my skull against a wall I
should've seen (bad solo strategy, wrong co-founder), and was about to
fall back into the same old job doing the same old stuff, living in
the same old place.

The only way I can convince myself my life is going in an interesting
direction (i.e. is worth living) is to do something drastic.

## I'm bored

Is this really what life is? Just going around doing the same types of
things until you die? The least I could do is change it up, a quarter
of a century in more or less one place is enough.

I know people who are gearing up to buy a house and settle down. I
could buy a house, but that feels more like tying a noose around my
neck - tying up all of my capital in a single asset I don't even
really need or want.

Once you have a house, that's it, you're tied down. The asset isn't
particularly liquid, so there's a barrier to escape.

Settling down sounds like something a corpse with lead weights tied to
it does at the bottom of a lake. People with partners and families see
things differently, no doubt, but I don't have those.


## I'm a practising neoliberal

It boils down to this. My religion, my ideology is liberalism. I truly
believe that immigration and the price system allow workers to be
allocated where they will be the most productive, and that workers
actually do follow these signals.

In order to practise what I preach, I have to look at what the labor
market is telling me and act accordingly.

## Other options I considered

I considered going to work for CERN, who I did interview with several
times while working at my job the first time around. Ultimately I gave
up on that once I ran the numbers and saw the effect on my lifetime
earnings. CERN pays well, but not as well as some companies in the US,
or even Switzerland.

I considered trying another startup, but unfortunately that felt like
more of the same. Founding a startup doesn't feel great a lot of the
time, and having the right co-founder for mutual support (and/or
shared delusion) is extremely valuable. And there's a huge pool of
investors and co-founders I haven't tapped, across the pond in the
US. I think the kind of borderline delusional conviction a startup
founder needs is more common in the US. It's something I used to have
and could build up again.

I also considered _working_ at a startup, but that would've delayed my
move to the US by at least a year. You need a year at the company to
L1B visa into the US, and my 2 years at Bloomberg still count despite
the gap. And I would've probably jumped at an okay looking offer just
to have the company go bankrupt a few months later.

## Conclusion

<https://www.youtube.com/watch?v=EEjq8ZoyXuQ>
