Why object to the death of Venice?
2022-08-23

While in Venice, I picked up a book, _If Venice Dies_ by Salvatore Settis. The
main thrust of the book is that tourism is bad, Venice has died or is on the
cusp of death, and changes need to be made to remedy the situation.

This is the kind of book that only works if you already agree with its premise,
and read it as outrage porn rather than as a well-motivated, well-explained
argument. That might seem a bit uncharitable, but the author makes a lot of
claims that are only backed up by vibes and impassioned rhetorical questions
like "Wouldn't that be a tragedy?" rather than any kind of reasoning.

There are two ways to look at each of the claims:

1. The author wants to enforce their vision of what Venice should be, and came
up with various justifications post hoc to make it seem like it's in everyone's
interests.

2. The author actually believes their own arguments, which largely amount to
fluff, vibes, and vaguely authoritarian policy prescriptions.

We can go through the book and see if (1) the sinister explanation or (2) the
naive explanation fits better.

There's of course the third explanation: that the author doesn't believe in any
of it and just wrote the book for a quick buck, which we'll ignore. Settis is a
lifelong archaelogist and art historian, so it's at least credible that he does
believe in the conclusions of the book.

Anyway, let's look at some of the unmotivated fluff. I don't expect this blog
post to be particularly entertaining or anything more than a rant.

<!--more-->

# Sometimes cities die

The first chapter is about the death of Athens, how it devolved into a barely
populated village and its history was lost:

> Yet nothing could be further from the truth: when Michael Choniates, who
> hailed from Constantinople, was appointed archbishop of Athens in the late
> twelfth century, he was astonished by the ignorance of the Athenians, who
> were unaware of their city's former glories, and weren't able to tell foreign
> visitors about their still intact temples, nor could they point out the
> places where Socrates, Plato, and Aristotle had preached their doctrines.
> 
> [...]
>
> When Athens was conquered by the Ottoman Turks in 1456-and the
> Parthenon-Church was turned into a mosque-the city even lost its name. What
> remained was a wretched village with a few huts scattered among the ruins,
> while the local population, which had been reduced to a few thousand, had
> started to call the city Satines-or Sethines-a bastardization that Rome was
> never subjected to.

This stuff is kind of sad. But it's hard to see what the problem is or what the
solution is - are there things missing from the Italian school curriculum that
the author wishes to add? Should schools in Venice teach more Venetian history?
These questions aren't asked or answered, so it's hard to understand the
solution the author is proposing.

I think the point of the chapter is that the author thinks some cities look
nice and he wants them to stay around after he dies:

> No: on the contrary, we the living should nurture beauty on a daily basis if
> we want some of it to survive, so that we may enjoy it and ensure its
> survival after our death.

He uses the word "we" instead of "I" for some reason, presumably as a device to
get us to agree with him.

There's no real argument or policy proposal made in this chapter, it appears to
just be here to set the tone - cities die. I don't think Venice is at risk of
dying so I consider this chapter to be irrelevant at best and an attempt at
emotional manipulation at worst.

# Population decline and tourism are bad

The tiny island of Venice used to have a population much larger than it does
right now, peaking at maybe 150,000 a few hundred years ago, declining to
50,000 today. Why is this a problem? The author doesn't explain.

> Although the extant demographic data is less reliable, the plague of 1348
> proved to be equally devastating, after which it's estimated that the
> population dropped from about 120,000 to 58,000: just a little over today's
> figures. Yet starting in the 1970s, a new kind of plague broke out in Venice.

Settis apparently thinks population decline due to people moving away or not
being born is in some way comparable to people literally dying of a plague.
That wouldn't be such a problem if there were some other argument about why
population decline is bad, but there isn't! The analogy of plague is used again
and again without any sense of proportion.

Even worse, apparently tourism has "devastated" Venice like a "bomb" and has
"annihilated" it. Someone should tell the tourists that the city has been
destroyed and there's nothing to look at.

Why is it so bad that the population of Venice has migrated to the mainland and
prefers to have more space, perhaps a bigger house and a garden, and so on?
Settis also thinks that a tourism monoculture is harmful but doesn't explain why:

> A tourist monoculture now dominates a city which banishes its native citizens
> and shackles the survival of those who remain to their willingness to serve.

This looks like it's about how Settis views it as beneath him to serve others
and assumes we share his opinion.  Unless he's a subsistence farmer, Settis is
"shackled" to serving others too, which he doesn't seem to realise.  There's also
a diatribe about how we must return Venice's people to Venice when evidently
they don't even want to live there.

He wants Venice's population to increase because he thinks it'd be cool, I guess.

By the way, a tourist monoculture might be bad (see: the pandemic) but it's by
far the most productive industry Venice can offer. Trying to shoehorn anything
else into Venice would reduce its prosperity greatly.

# Big cities are bad

The author decries megalopolises and talks about how every city seems destined
to become some kind of megacity like Tokyo or Shanghai.

I did enjoy how Settis mentioned Trantor from Asimov's _Foundation_ series,
it's one of my favourites. It makes sense that Settis would have read it,
considering it's about the fall of a civilisation and the abandonment of a
city.

> A perverse continuity has established itself between the megalopolises and
> the shanty towns. Thus, was Isaac Asimov's ecumenopolis-the city-planet of
> Trantor featured in the science fiction Foundation series which numbered 40
> billion inhabitants-a nightmare or a prophecy?

I don't know, I think it's neither, maybe it's a dream. This chapter has one of
the worse examples of the repeated rhetorical question form of "argument":

> Yet do we really want to think of this phenomenon as inevitable and assume it
> will conquer the world, supplanting all other urban forms? Or would it
> instead be worthwhile to keep other alternatives in mind when we think of the
> city of the future, analyzing its characteristics and effects on history and
> the notion of livability in our present times? Do we wish to nurture or
> destroy the multiplicity and diversity of urban forms?

Reading this is hilarious if you come up with different answers to the author.
I consider large cities very livable, I'm even moving to New York.

There's another whole chapter about how skyscrapers are bad because the author
thinks they're ugly and stupid. I agree that the government subsidising
skyscraper construction as a vanity project is stupid, but that's not at all
the angle Settis takes.

> The acropolis of skyscrapers dominates the historic city from its heights,
> having situated itself there in a commanding position that calculatedly
> relegates the historic city to the sidelines.

He feels that the historic city, which hasn't been destroyed, is being
humiliated in some way, and that's enough for him to ban them. Great. There
can't be any supporting evidence for this since it's inherently subjective.

There's this bit which may be some kind of projection of Settis's own
insecurities onto Venice:

> But could Venice eventually be surrounded by a ring of skyscrapers, as
> envisioned in Aqualta, or will its historic center be dwarfed by a single
> high-rise, turning the former into an old dwarf who gets stared down by a
> young, muscled giant?

Is Settis really the old dwarf? What a bizarre analogy. The single high-rise
he's talking about here is a tower on the mainland, in a derelict industrial
zone, not the island itself.  You'd barely be able to see it from the island.

# Money matters, or does it?

This is really where the book starts to take a turn. Previously Settis limited
himself to talking about how the city doesn't conform to his desires, and how
he finds tall buildings ugly (except if old), but now he starts making economic
statements.

> We should counter the approximations of self-styled appraisers with the
> seriously pondered reflections of others on the true value of cultural
> heritage. One need only cross the Alps and head into France. A report
> entitled The Economy of the Immaterial: The Growth of Tomorrow, which was
> drawn up by Maurice Levy and Jean-Pierre Jouyet, reflects on immaterial
> values (meaning priceless ones) as the basis of all future growth.
>
> [...]
>
> The report was commissioned by the French Ministry of the Economy in 2006
> under the presidency of Jacques Chirac and concluded that immaterial values
> are "concealing a huge potential for growth, which can stimulate the French
> economy by generating hundreds of thousands of jobs, while simultaneously
> preserving others that would otherwise be put at risk."

So immaterial values are good because they can stimulate the economy and create
jobs. Isn't that _exactly_ what's going on right now? The immaterial value of
Venice is creating jobs and income! But Settis condemns that, he only approves
of certain kinds of jobs and income, I guess.

Or should we just ignore that report? Literally the next paragraph says:

> Yet Venice is now threatened by what John Maynard Keynes once termed the
> "parody of an accountant's nightmare," in other words the abject, prejudiced
> view that everything should have a price tag, or better yet, that money is
> the only thing that matters:

This is pretty much incoherent. Immutable values will produce lots of money,
except money doesn't matter. Sure, but I don't see what we should take away
from that.

# Policy

It goes on and on, there is a ton of fluff about how Venice has been humiliated
and degraded by having copies of it made, and how theme parks have gondolas in
them, but the real meat of the book comes when Settis makes some policy
recommendations.

## No development for tourism

Settis says he wants what's best for Venice's destiny, but that seems to be
code for conforming to what he deems acceptable, and he doesn't want tourism:

> Venice must know how to creatively construct its own destiny, tailoring each
> change it makes according to the best possible future for its citizens, and
> not what the tourists or real estate agencies want.

This implies that tourism isn't best for Venice, but never backs that up.

## Ban development completely, actually

Settis supports plans to ban development in a belt around cities' historic
limits due to some idea of the importance of the meeting of city and
countryside:

> It's time to "limit the endless expansion of suburban sprawl by returning
> cities to their margins," as Zanardi has written, while at the same time
> "soldering the historic center to its periphery" and reestablishing the
> connection between the city and its citizens.

Luckily this proposal can't apply to Venice since it has been naturally limited
by the lagoon.

## Enshrine a right to the city

He supports a right to the city, similar to the one Brazil recognised, which:

> guarantee[s] the right to sustainable cities, understood as the right to
> urban land, housing, environmental sanitation, urban infrastructure,
> transportation and public services, to work and leisure for current and
> future generations; democratic administration by means of participation of
> the population and of the representative associations of the various segments
> of the community in the formulation, execution and monitoring of urban
> development projects, plans and programs.

This is incredibly vague but is presented as Settis as a panacea. It's nothing
but a tool to block any and all development. Settis claims he doesn't want to
block all progress, but only advocates laws and "rights" that'll enable all
progress to be blocked.

The wishy-washiness of this part of the book is exemplified:

> Even in Venice, the health of democracy is determined by how successful
> citizens prove in defending their rights, which include the common good and
> the social functions of property.

The social functions of property! What does that actually mean? Who gets to
decide what the social functions of property are? Its owners or others? People
voting to use the coercive power of the state to restrict what property owners
can do is not moral unless the owner's plans will materially harm them. Having
to look at a building on the horizon is not harmful in the way that a polluting
factory is.

## Tourism is bad

Yes, this same proposal to have the government distort things away from tourism
crops up again:

> In Venice's case, the work available to local residents can't be restricted to
> the tourist monoculture, but has to be worthy of the immense civic capital
> the city has accumulated over centuries. The social function of property,
> regardless of its ownership, can't be solely determined by increasing real
> estate value while decimating the local population and condemning the city to
> die. It must nurture creative and productive enterprises, repopulate the city
> with young people, and loosen the tourist monoculture's stranglehold.

The work isn't restricted to the tourist monoculture. Anyone is free to do
whatever they want. But if they don't generate enough income to pay the bills,
they'll have to leave.

The only solution that will eliminate the tourist monoculture is for the
government to subsidise other industries and displace tourist-oriented
industries. Why is that necessary? Because tourism is not worthy of Venice!
Catering to tourists is unworthy of Venetians! This is enough to justify taking
money from productive Italians and using it to subsidise Venetians to do
something other than serve tourists.

## An architectural code of ethics

Settis wants architects to have to take a Vitruvian Oath, like the Hippocratic
Oath, wherein architects swear not to design ugly buildings. Yes, I'm serious.

> We could easily adopt every single one of the professional requirements that
> Vitruvius lists in his book and compile them into a "Vitruvian Oath," turning
> it into the perfect equivalent of the Hippocratic Oath.

> [...]

> If those who build in Venice knew how to marry practice and theory, no
> architectural design would so flagrantly ignore the physical conditions and
> construction practices unique to that city.

The various elements of the proposed oath are extremely vague and impossible to
criticise, except in how vague they are.

## We should encourage people to live in Venice

Right at the end of the book, we get a very confused chapter which describes
immense corruption in the MOSE project to protect Venice from flooding, then
proceeds to advocate a range of subsidies and tax breaks the government should
deploy to revive Venice:

> In Venice's case, this new pact will have to begin from a strong sense of
> commitment to spur politicians and public institutions to adopt a more
> creative outlook toward the city, to bring the historic city back to life and
> gear it toward the future, the means to create a new kind of politics to stem
> the perverse logic causing the exodus of citizens, and to encourage the young
> to remain via strong incentives such as tax breaks. It would also mean
> curbing the rampant proliferation of second homes and the transformation of
> buildings into nothing more than hotels. It would mean encouraging
> manufacturing and private enterprise as well as generating opportunities for
> a wider range of creative jobs. It would mean reunifying the historic city,
> lagoon, and mainland by differentiating their functions, making more
> agricultural land available and investing in new fisheries, reutilizing old,
> vacant buildings, incentivizing research, launching new professional training
> schemes and apprenticeships and investing in universities, chiefly by making
> it affordable for students to actually live in the city. It would mean
> developing new models, analyzing situations, evaluating options, and
> emphasizing initiatives of a higher caliber (like the universities and the
> Biennale) and not just enslaving the city to "uncontrollable market forces."
> It would mean enshrining the right to the city and the common good as our
> first priority.

Why is manufacturing specifically singled out as a "good" kind of enterprise?
Why are second homes bad? Why are hotels bad? Why are creative jobs good? Who
is going to re-utilize the abandoned buildings, when they have been abandoned
due to being useless? Why is it inherently good for students to live in Venice?

This list of proposals is unmotivated and unexplained.

# Conclusion

Settis wants the government to ban stuff he doesn't like (tourism, tall
buildings, subways) and subsidise stuff he does like (creatives, people living
in Venice). He finds tourism degrading and would prefer Venice be a military
(yes, he explicitly uses the word "military") and economic powerhouse like it
was a thousand years ago.

As usual with these kinds of people, they get a raft of academics from their
bubble to rant and rave about how great their book is, ignoring that it only
works if you already agree with the conclusions.

The only motivations given are hyperbolic, borderline insane analogies about
how skyscrapers are muscled young men overlooking dwarves, and how someone
moving out of Venice is equivalent to being killed by a plague.

Truly crazy stuff. I guess I bought the book, so the author wins in the end.
