Booting the 1994 Dr Dobb's 386BSD 1.0 CD
2023-06-19

386BSD 1.0 was released in 1994 on a CD in an issue of Dr Dobb's Journal. There
are guides on the internet on how to boot 386BSD 1.0 in QEMU, like
<http://gunkies.org/wiki/Talk:Installing_386BSD_1.0_on_Qemu> but I don't think
there are any guides on how to boot it like someone in 1994 would've booted it,
from a real MS-DOS installation.

Rather funnily, 386BSD is listed as "theoretically bootable" here:
<https://gunkies.org/wiki/386BSD_1.0>. And there's a post on WinWorld saying
"Personally I have no idea how to boot it (honestly don't ask)" with no
elaboration:
<https://forum.winworldpc.com/discussion/13240/offer-386bsd-reference-cd-rom>.

It's time to put theory into practice and work out how to boot this OS.
There are a couple of things I want to try:

1. DOSBox - maybe it'll work?
2. QEMU with MS-DOS 6.22
3. The instructions from gunkies.org

You can download the CD image here: <https://archive.org/details/386BSD1.0> and
follow along. 

Also, RIP Bill Jolitz.

<!--more-->

## Poking around the CD image

Download that ISO above, then mount it:

```
$ sudo mount 386BSD-1.0.iso /mnt
$ ls /mnt
386bsd        arch      cd            etc       nbsd         setup1.ex_    SOFTSUB.TXT  vbrun200.dl_
386bsd.ddb    b         CONTRIB.TXT   INFO.TXT  RELEASE.TXT  setup.exe     tmp
386bsd.small  bin       COPYRGHT.TXT  install   root         setupkit.dl_  usr
a             boot.exe  dev           mnt       sbin         setup.lst     var
```

A few things to notice:

* There are some EXEs around. We're probably supposed to run boot.exe from DOS:
  ```
  $ file boot.exe
  boot.exe: MS-DOS executable
  ```

* And setup.exe from Windows:
  ```
  $ file setup.exe 
  setup.exe: MS-DOS executable, NE for MS Windows 3.x (EXE)
  ```

* There are some BSD kernels. We can tell because they're executables and have
  a bunch of kernel-looking strings in them:
  ```
  $ file 386bsd
  386bsd: a.out little-endian 32-bit demand paged pure executable not stripped
  $ strings 386bsd
  ...
  /sbin/init
  ...
  %s: blkdev %d too big, not configured.
  %s: blkdev %d already used by %s, not configured.
  devif: config %s blkdev
  %s: chrdev %d too big, not configured.
  ```
  These look like the messages you get when booting e.g. OpenBSD, which is
  a descendent of 386BSD.

* There don't seem to be any instructions on the CD itself. We're probably
  supposed to read the magazine.

## Reading the instructions

I cannot find any instructions anywhere. This section is a placeholder. If I
find the issue of Dr Dobb's Jorunal or the instructions that came with the CD,
I'll make another post.

## Trying to boot it from DOSBox

First, install DOSBox:

```
$ sudo apt install dosbox
```

You need to change some settings in your DOSBox config which, for me, lives at
`~/.dosbox/dosbox-0.74-3.conf`. This is because `boot.exe` has a faulty (or
maybe it's not faulty) Windows detection routine which aborts when you have
certain DOSBox settings. A failed boot looks like this:

```
Z:\> imgmount E ~/tmp/3.iso -t iso
Z:\> E:
E:\> boot 386bsd
Text 466944
Data 20480
Start 0xfe000000
Cannot run from Windows DOS Shell
```

If you make sure the `[dos]` section looks like this:

```
[dos]
#            xms: Enable XMS support.
#            ems: Enable EMS support.
#            umb: Enable UMB support.
# keyboardlayout: Language code of the keyboard layout (or none).

xms=false
ems=false
umb=false
keyboardlayout=auto
```

Then you can get a bit further:

```
Text 466944
Data 20480
Start 0xfe000000
can't open emm
386BSD Release 1.0 by William & Lynne Jolitz [1.0.22 10/27/94 15:32]
```

After this we see some concerning probing messages:

```
probing for wd port 1f0
probing for fd port 3f0
```

And these probes don't find anything. It seems like the hardware DOSBox
presents to the 386BSD kernel is too fake and too different from what's
supported, meaning we can't boot.

```
panic: cannot mount root
press key to boot/dump
```

I don't think I'll be able to get this to work.

## Running setup.exe

This is interesting, there's a setup.exe that does something mysterious. I
installed Windows 3.11 into an MS-DOS 6.22 VM using QEMU and ran setup.exe to
see what would happen.

A setup wizard runs and installs a bunch of manuals! This is what it looks
like:

![Manuals!](/static/386bsd1.png)
![Manuals!](/static/386bsd2.png)

Kind of cool, but this doesn't actually help us run 386BSD.

## Attempt 2: MS-DOS 6.22 in QEMU

I got a bit further with this but spoiler alert: I didn't actually manage to
boot to a shell with this either.

Install QEMU:

```
$ sudo apt install qemu-system-x86
```

Get MS-DOS 6.22 install disk images: <https://winworldpc.com/product/ms-dos/622>.

Make a disk image:

```
$ qemu-img create msdos.disk 2G
```

2G is the maximum MS-DOS partition size.

Boot the first floppy and follow the instructions:

```
$ qemu-system-i386 -hda msdos.disk -fda disk1.img
```

When asked to swap the floppy, press CTRL-ALT-2, you'll see the QEMU monitor
prompt:

```
QEMU 6.2.0 monitor - type 'help' for more information
(qemu)
```

Run this to change the floppy:

```
(qemu) change floppy0 disk2.img
```

And press CTRL-ALT-1 to switch back to the MS-DOS installer. Continue until
MS-DOS is installed.

Get the MS-DOS CD Extensions:
<https://winworldpc.com/product/ms-cd-extensions-msc/125> and install them by
running setup.exe. This is so you can use your CD drive.

```
$ qemu-system-i386 -hda msdos.disk -fda mscdex.img
```

In DOS:

```
C:\> A:
A:\> install
```

Then it'll hang for a few minutes, but that's fine. Remove the floppy and
reboot.

Now start the VM with the CD inserted:

```
$ qemu-system-i386 -hda msdos.disk -cdrom 3.iso
```

You'll see the CD-ROM driver stuff on boot:

```
Booting from Hard Disk...
Starting MS-DOS...


HIMEM is testing extended memory...done.


----------------------------------------------------------------
|         E-IDE/ATAPI  CD-ROM device driver,  Ver 1.25         |
| Copyright (C) LG Electronics Inc. 1997. All rights reserved. |
----------------------------------------------------------------
Unit 0:  QEMU      QEMU DVD-ROM      Product Rev.: 2.5+
Transfer Mode      : Programmed I/O


C:\>C:\DOS\SMARTDRV.EXE /X
MSCDEX Version 2.23
Copyright (C) Microsoft Corp. 1986-1993. All rights reserved.
Drive D: = Driver MSCD000 unit 0
C:\>
```

There's a problem, the CD drivers and stuff take up too much conventional
memory, and we can't boot 386BSD:

```
D:\>boot 386bsd
boot: need 23632 more bytes of conventional memory
boot: cannot allocate enough DOS program memory - reduce DOS size
```

The problem is that a whopping 101K of conventional memory is filled:

```
D:\>mem 

Memory Type        Total  =   Used  +   Free
----------------  -------   -------   -------
Conventional         639K      101K      538K
Upper                  0K        0K        0K
Reserved               0K        0K        0K
Extended (XMS)    64,512K    2,112K   62,400K
----------------  -------   -------   -------
Total memory      65,151K    2,213K   62,938K

Total under 1 MB     639K      101K      538K
```

We can fix this by using EMM386 to move some stuff out of conventional memory.
Make config.sys look like this:

```
DEVICE=C:\DOS\HIMEM.SYS
DEVICE=C:\DOS\EMM386.EXE NOEMS
DOS=HIGH,UMB
FILES=30

LASTDRIVE=Z
DEVICEHIGH=C:\CDROM\GSCDROM.SYS /D:MSCD000 /
```

The key is that the CD driver and as much of DOS as possible needs to live in
upper memory. This results in much less used conventional memory:

```
Memory Type        Total  =   Used  +   Free
----------------  -------   -------   -------
Conventional         640K       24K      616K
Upper                 99K       81K       18K
Reserved             384K      384K        0K
Extended (XMS)    64,413K    2,353K   62,060K
----------------  -------   -------   -------
Total memory      65,536K    2,842K   62,694K

Total under 1 MB     739K      105K      634K

Largest executable program size       616K (630,864 bytes)
Largest free upper memory block        18K  (18,048 bytes)
MS-DOS is resident in the high memory area.
```

And now we can boot 386BSD:

```
D:\>boot 386bsd
Text 466944
Data 20480
Start 0xfe000000
Warning: Too little RAM memory, running in degraded mode.
panic: pmap_ptalloc: kernel pmap
press key to boot/dump
```

We need to boot QEMU with more memory (8M should be plenty):

```
$ qemu-system-i386 -hda msdos.disk -cdrom 3.iso -m 8
```

Trying to boot again:

```
D:\>boot 386bsd
Text 466944
Data 20480
Start 0xfe000000
386BSD Release 1.0 by William & Lynne Jolitz. [1.0.22  10/27/94 15:32]
Copyright (c) 1989-1994 William F. Jolitz. All rights reserved.
clk:  irq0
pc: pc0 <color> port 60 irq1
aux:  port 310 irq12
wd: wd0 <QEMU HARDDISK>  port 1f0 irq14
fd: fd0:  port 3f0 irq6 drq2
com: com1: fifo port 3f8 irq4
lpt: lpt0  port 378 irq7
npx: npx: irq13
mcd:  port 300 irq10
wd0: cannot find label (no disk label)
panic: cannot mount root
press key to boot/dump
```

Now we're getting somewhere - 386BSD has scanned and detected the hard disk,
although there's no BSD disklabel.

## Trying the Gunkies instructions

At this point I'm curious as to whether the Gunkies instructions work:
<http://gunkies.org/wiki/Talk:Installing_386BSD_1.0_on_Qemu>.

And they do!

```
$ qemu-system-i386 -fda ddbboot.img -hda disk -hdb 3.iso -m 8
A:\>boot 386bsd.ddb wd1d
Text 335872
Data 114688
Start 0xfe000000
can't open emm
386BSD Release 1.0 by William & Lynne Jolitz. [1.0.21  10/27/94 14:23]
Copyright (c) 1989-1994 William F. Jolitz. All rights reserved.
clk:  irq0
pc: pc0 <color> port 60 irq1
aux:  port 310 irq12
wd: wd0 <QEMU HARDDISK> wd1 <QEMU HARDDISK>  port 1f0 irq14
fd: fd0: 1.44M port 3f0 irq6 drq2
com: com1: fifo port 3f8 irq4
lpt: lpt0  port 378 irq7
npx: npx: irq13
mcd:  port 300 irq10
erase ^?, kill ^U, intr ^C
```

But the install hangs when partitioning the disk, when we try to create the
backup superblocks:

```
# ./install
...
super-block backups (for fsck -b #) at:
32, 16224, 32416, 48608, 64800, 80992, 97184,
```

Then it just hangs.

## Conclusion

Getting 386BSD 1.0 working is much more difficult than 0.1 and 0.2, which are
much more well studied and patched.

There may be a problem with newer versions of QEMU, specifically SeaBIOS. It
looks like all of the guides which end in success involve compiling e.g. QEMU
0.12.3 and using the PC BIOS included with that version.

If I do something non-trivial on 386BSD like compiling a program or running an
FTP server, I'll make another post. Right now, I've almost got the installer to
work, which is a somewhat unsatisfying place to stop.
