Book Review: Children of Memory
2024-04-02

I recently read Children of Memory by Adrian Tchaikovsky. It's the third in the
Children of Time series. Each book in the series shares a common backstory and
general template:

* Humanity sends out terraforming missions to the stars.

* Earth collapses into war and disaster over hundreds of years.

* Survivors send out ark ships to find and hopefully colonise the Edens waiting
  for them.

* It doesn't go as planned: uplifted spiders are waiting for them, alien
  parasites, spacefaring octopuses, reality-bending alien technology does
  something weird.

I think this book was a bit disappointing and I'll explain why. Heavy spoilers,
and this "review" makes no sense if you haven't read the book.

<!--more-->

### Tell, don't show

I really like the trope of a farm girl/boy living their normal, mundane life on
a farm, and slowly discovering things aren't as they appear. I wish this book
had more of that and that we learned what was going on _with_ the characters on
the colony rather than literally being shown what happened with the initial ark
ship and colonisation through time skips.

This is almost the opposite of the usual complaint: people want books to show
not tell. I think it would've been more engaging for us to learn about a hazy,
barely remembered past through a little girl learning an oral history in a one
room schoolhouse.

There was a great novel by Stephen Baxter with this kind of setup:
[Flux](https://en.wikipedia.org/wiki/Flux_\(novel\)). A nomadic tribe of people
are living inside a neutron star, but they don't know that. You work out what's
really going on as they do. Baxter also did this kind of thing with
[Raft](https://en.wikipedia.org/wiki/Raft_\(novel\)). I recommend both books!
The setup is much weirder and sci-fi than the Children of Memory colony in both
cases.

I tend to like books where you really have no clue what's going on. I realise
this isn't everyone's preference - I've even seen Children of Memory reviews
where people say it was too confusing and didn't explain what was going on
enough.

### More crow or less crow?

I liked the sentient crows but the story of how they gained sentience and the
history of their society was nothing compared to the detailed treatment the
spiders got in Children of Time or even what the octopuses got in Children of
Ruin. I didn't really just want more of the crows necessarily, but injecting
some crows as side characters with a shitty little side history means they feel
like a gimmick at best.

Some possible solutions:

1. Make the crows a central part of the story rather than something that could
   just slot in or out. Spend much more time on them.
  
2. Remove them and have a separate book with a more detailed history and a
   different plot entirely.

3. Have them in the story as e.g. the Witch's familiars, but don't explain
   them.

I'm partial to (3). Have the crows, don't explain them. I don't know if we need
every book to be a retread of Children of Time with a different species.

### Missed opportunity for a depressing ending

The depressing and hopeless ending of the colony was great. Liff being a
desperate little girl who was simply the last person to starve to death really
tugged at my heart strings. I wish the book had the guts to end on this
hopeless, soul-crushing note. Short stories ending on that kind of note have
always been my favourite.

An example of a story with a memorably despairing tone is [The
Star](https://en.wikipedia.org/wiki/The_Star_\(Clarke_short_story\)) by Arthur
C Clarke. It's only a few pages long and I highly recommend finding it online
and reading it. Very saddening.

Children of Memory hits you with a one-two punch of cop outs - first it was
actually a simulation, then second Liff is actually brought out of the
simulation into a cloned body and her pleas for food to eat are actually answered. 

Totally unnecessary.

### We get it already! Stop with the time loops!

Once we work out that there's a time loop or simulation, or something, we still
go through another couple of iterations of the loop for no real reason. It's
like if in the classic [Star Trek: The Next Generation episode Cause and
Effect](https://en.wikipedia.org/wiki/Cause_and_Effect_\(Star_Trek:_The_Next_Generation\)),
there were an extra two or three iterations of the time loop for 40 minutes in
the middle of the episode for no reason.

Keep it tight! One loop where you don't know it's a loop and all of the context
is set up, one loop where things are a bit weird and characters realise
something is going on, then everything gets resolved.

Like I said, my preferred resolution would be that the colony was real,
everyone died depressing and pointless deaths after a meagre existence. They
tried their best and they failed.

This story seems like a much less emotionally impactful version of [The Inner
Light](https://en.wikipedia.org/wiki/The_Inner_Light_\(Star_Trek:_The_Next_Generation\))
- instead of "it all happened long ago and none of them can be saved" we get
"it's a simulation in a loop, let's clone all of the people and live together
in a happy utopia".

I don't even get why cloning Liff is treated like saving her - those hundreds
of iterations of death and suffering still happened and are _still happening at
the end_! If the point is that simulations are sentient too, then cloning the
simulations into real bodies doesn't do anything to save the simulations!

### Kill the parasite!

I'm one of those people who thought the bad guys won at the end of Children of
Ruin - the parasite is everywhere and could take control at some future date
when it changes its mind. I do not for a second relate to it at all, I don't
feel sorry for it when others distrust it, and I think the war against the
parasite should never have ended.

There's a scene where Miranda hugs "Miranda" and consoles the parasite, saying
it did a good job, I hated it. This is even after a scene where the parasite
admits to itself it might take control of everyone at some point in the future.
i.e. peoples' fears about it are rational.

I think the parasite perspective is interesting, but no-one else should even be
remotely okay with it existing. Totally irrational.

I was also annoyed by a lot of "Miranda's" inner monologue, it felt very
repetitive. Something being nonsense I can forgive, something being repetitive
and a chore to read I can't forgive.

### Verdict

Children of Time is really neat and has lots of cool ideas.

Children of Ruin has some cool ideas, a well done shift into horror halfway
through, then a disappointing ending. Still well worth a read overall.

Children of Memory was just riddled with missed opportunities and I didn't like
it nearly as much. I don't recommend reading it.
